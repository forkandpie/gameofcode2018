socketio = require('socket.io'),
http = require('http'),
SerialPort = require("serialport")

var socketServer;
var serialPort;
var portName = 'COM4';
var sendData = "";

function startServer(debug) {
	serialListener(debug);
	initSocketIO(debug);
}

function initSocketIO(debug) {
	socketServer = socketio.listen(1337);
	if (!debug) {
		socketServer.set('log level', 1);
	}
	socketServer.on('connection', function (socket) {
		console.log("user connected");
		socket.on('paper', function(data) {
			console.log("paper");
			serialPort.write('P');
		});
		socket.on('plastic', function(data) {
			console.log("plastic");
			serialPort.write('T');
		});
		socket.on('glass', function(data) {
			console.log("Glass");
			serialPort.write('G');
		});
	
    });
}

// Listen to serial port
function serialListener(debug) {
    var receivedData = "";
    serialPort = new SerialPort(portName, {
        baudRate: 9600,
        // defaults for Arduino serial communication
        dataBits: 8,
        parity: 'none',
        stopBits: 1,
        flowControl: false
    });
 
    serialPort.on("open", function () {
      console.log('open serial communication');
        // Listens to incoming data
        serialPort.on('data', function(data) {
            receivedData += data.toString();
	        if (receivedData .indexOf('E') >= 0 && receivedData .indexOf('B') >= 0) {
        		sendData = receivedData .substring(receivedData .indexOf('B') + 1, receivedData .indexOf('E'));
           		receivedData = '';
         	}
        	// send the incoming data to browser with websockets.
       		socketServer.emit('update', sendData);
    	});
    });
}

startServer(false);
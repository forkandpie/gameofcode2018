#include <Servo.h>

Servo myservo;  // create servo object to control a servo

// LED vars
const int tledPin = 8;
const int pledPin = 11;
const int startledPin = 9;
const int gledPin = 12;
const int servoPin = 13; // analog pin used to connect the servo
int val; // variable to read the value from the analog pin

// LED read vars
char inputString ;         // a string to hold incoming data
boolean toggleComplete = false;  // whether the string is complete

// Potmeter vars
const int analogInPin = A0;
int sensorValue = 0;        // value read from the potmeter
int prevValue = 0;          // previous value from the potmeter
int timeout = 1000;

void setup() {
  // initialize serial:
  Serial.begin(9600);

  // init LEDS
  pinMode(tledPin, OUTPUT);
  pinMode(pledPin, OUTPUT);
  pinMode(gledPin, OUTPUT);
  pinMode(startledPin, OUTPUT);
  digitalWrite(tledPin, LOW);
  digitalWrite(pledPin, LOW);
  digitalWrite(gledPin, LOW);
  digitalWrite(startledPin, HIGH);

  //init Servo
  //digitalWrite(startledPin, 0);
  myservo.attach(servoPin);
  val = map(val, 0, 1023, 60, 60);     // scale it to use it with the servo (value between 0 and 180)
      myservo.write(val);
}

void loop() {
  // Recieve data from Node and write it to a String
  while (Serial.available() && toggleComplete == false) {
    char inChar = (char)Serial.read();
    if (inChar == 'E' || inChar == 'T' || inChar == 'P' || inChar == 'G') { // end character for led
      toggleComplete = true;
      inputString = inChar;
    }

    //inputString += inChar;
    //if(inChar == 'E'){ // end character for led
    //  toggleComplete = true;
    // }
    // if(inChar != 'P'){
    //   inputString += inChar;
    //}
  }
  // Toggle LED's
  if (!Serial.available() && toggleComplete == true)
  {
    digitalWrite(startledPin, LOW);
    //char inChar = (char)Serial.read();
    if (inputString == 'T')
    {
      digitalWrite(tledPin, HIGH);
      val = analogRead(servoPin);            // reads the value of the servo (value between 0 and 1023)
      val = map(val, 0, 1023, 115, 115);     // scale it to use it with the servo (value between 0 and 180)
      myservo.write(val);                  // sets the servo position according to the scaled value
      delay(timeout);                      // waits for a 2 second's
      val = map(val, 0, 1023, 60, 60);     // reset servo
      myservo.write(val);
      digitalWrite(tledPin, LOW);
      digitalWrite(startledPin, HIGH);
    }
    else if (inputString == 'P')
    {
      digitalWrite(pledPin, HIGH);
      val = analogRead(servoPin);            // reads the value of the servo (value between 0 and 1023)
      val = map(val, 0, 1023, 150, 150);     // scale it to use it with the servo (value between 0 and 180)
      myservo.write(val);                  // sets the servo position according to the scaled value
      delay(timeout); // waits for a 2 second's
      val = map(val, 0, 1023, 60, 60);     // reset servo
      myservo.write(val);                  // sets the servo position according to the scaled value
      digitalWrite(pledPin, LOW);
      digitalWrite(startledPin, HIGH);
    }
    else if (inputString == 'G')
    {
      digitalWrite(gledPin, HIGH);
      val = analogRead(servoPin);            // reads the value of the servo (value between 0 and 1023)
      val = map(val, 0, 1023, 180, 180);     // scale it to use it with the servo (value between 0 and 180)
      myservo.write(val);                  // sets the servo position according to the scaled value
      delay(timeout); // waits for a 2 second's
      val = map(val, 0, 1023, 60, 60);     // reset servo
      myservo.write(val);                  // sets the servo position according to the scaled value
      digitalWrite(gledPin, LOW);
      digitalWrite(startledPin, HIGH);
    }
    toggleComplete = false;
  }
  // Potmeter
  sensorValue = analogRead(analogInPin);
  // read the analog in value:
  if (prevValue != sensorValue) {
    Serial.print("B"); // begin character
    Serial.print("E"); // end character
    prevValue = sensorValue;
  }
  delay(50); // give the Arduino some breathing room.
}

# Automated garbage sorter [Repository](https://bitbucket.org/forkandpie/gameofcode2018/src/1c794aa93566?at=master)

# Presentation
https://prezi.com/j-m2_6a0zrqq/automated-garbage-sorter/?utm_campaign=share&utm_medium=copy

# Installation

Install node js https://nodejs.org/en/download/
```
$ npm install
$ node server
$ npm run
```

# Running (dev)

```
$ node index.js
```

Go to: `http://localhost:9966/`

# License

Licensed under MIT License.
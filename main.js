// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import {KNNImageClassifier} from 'deeplearn-knn-image-classifier';
import * as dl from 'deeplearn';
import io from 'socket.io-client';

// Number of classes to classify
const NUM_CLASSES = 7;
// Webcam Image size. Must be 227. 
const IMAGE_SIZE = 227;
// K value for KNN
const TOPK = 10;
// Minimum confidence ratio to raise material detection
const MIN_CONFIDENCE = .71;
// Materials
const MATERIALS = [null, 'plastic', 'glass', 'paper'];

const I_EMPTY = 0;
const I_CAN = 1;
const I_BOTTLE = 2;
const I_CUP = 3;
const I_CUP2 = 4;
const I_NAPKIN = 5;
const I_NAPKIN2 = 6;

const MATERIAL_MAP = 
{
  2: 'glass',
  1: 'plastic',
  3: 'paper',
  4: 'paper',
  5: 'paper',
  6: 'paper'
};

class Main {
  constructor() {
    // Initiate variables
    this.infoTexts = [];
    this.training = -1; // -1 when no class is being trained
    this.videoPlaying = false;
    this.classConfidences = [];
    this.lastClass;

    // Initiate deeplearn.js math and knn classifier objects
    this.knn = new KNNImageClassifier(NUM_CLASSES, TOPK);
    window.knn = this.knn;
    // Create video element that will contain the webcam image
    this.video = document.createElement('video');
    this.video.setAttribute('autoplay', '');
    this.video.setAttribute('playsinline', '');
    
    // Add video element to DOM
    document.body.appendChild(this.video);
    
    // Create training buttons and info texts    
    for (let i=0;i<NUM_CLASSES; i++) {
      const div = document.createElement('div');
      document.body.appendChild(div);
      div.style.marginBottom = '10px';

      // Create training button
      const button = document.createElement('button')
      button.innerText = "Train "+i;
      div.appendChild(button);

      // Listen for mouse events when clicking the button
      button.addEventListener('mousedown', () => this.training = i);
      button.addEventListener('mouseup', () => this.training = -1);
      
      // Create info text
      const infoText = document.createElement('span')
      infoText.innerText = " No examples added";
      div.appendChild(infoText);
      this.infoTexts.push(infoText);
    }
    
    this.createTestButton("Glass");
    this.createTestButton("Plastic");
    this.createTestButton("Paper");

    // Setup webcam
    navigator.mediaDevices.getUserMedia({video: true, audio: false})
    .then((stream) => {
      this.video.srcObject = stream;
      this.video.width = IMAGE_SIZE;
      this.video.height = IMAGE_SIZE;

      this.video.addEventListener('playing', ()=> this.videoPlaying = true);
      this.video.addEventListener('paused', ()=> this.videoPlaying = false);
    })
    
    /*
    // Load knn model
    this.knn.load()
      .then(() => this.start()); 
    */
    
    this.initSocket();

    // const videofiles = ['empty2.mp4', 'can22.mp4'];
    // const itemclasses = [I_EMPTY, I_CAN];
    const videofiles = ['empty4.mp4', 'can4.mp4', 'bottle4.mp4', 'cup41.mp4', 'cup42.mp4'];
                        // 'empty3.mp4', 'can3.mp4', 'bottle3.mp4', 'cup31.mp4', 'cup32.mp4'];
    const itemclasses = [I_EMPTY, I_CAN, I_BOTTLE, I_CUP, I_CUP2];
                        //  I_EMPTY, I_CAN, I_BOTTLE, I_CUP, I_CUP2];
    
    var videoCounter = 0;
    var videoElement = document.createElement('video');
    var timerId;
    var iosocket;
    
    videoElement.width = IMAGE_SIZE;
    videoElement.height = IMAGE_SIZE;
    videoElement.onended = function() {videoCounter++; getNextVideo();};
    //document.body.appendChild(videoElement);
    
    getNextVideo();
    timerId = setInterval(function() {
      console.log(videoCounter + " " + videofiles[videoCounter] + " " + itemclasses[videoCounter]);
      window.knn.addImage(dl.fromPixels(videoElement), itemclasses[videoCounter]);
    }, 300);

    function getNextVideo() {
      if (videoCounter == videofiles.length) {
        clearInterval(timerId);
        return;
      }
      videoElement.src = "video/" + videofiles[videoCounter];
      videoElement.play();
    }
  }
  
  createTestButton(text) {
    const div = document.createElement('div');
    document.body.appendChild(div);
    div.style.marginBottom = '10px';

    // Create test button
    const button = document.createElement('button')
    button.innerText = "Test "+ text;
    div.appendChild(button);

    // Listen for mouse events when clicking the button
    button.addEventListener('click', () => {
      if (this.iosocket != null) {
        this.iosocket.emit(text.toLowerCase());
      }
      else {
        console.info("Socket no connected");
      }
    });
  }

  initSocket() {
		this.iosocket = io('http://localhost:1337/');
		this.iosocket.on('connect', () => {
      console.log('Socket connected');
      // Load knn model
      this.knn.load()
        .then(() => this.start()); 
	  });
		this.iosocket.on('disconnect', () => {
      console.log('Socket disconnected');
      this.iosocket = null;
      stop();
	  });
		this.iosocket.on('error', (err) => {
      console.log('Socket error', err);
      this.iosocket = null;
      stop();
	  });
  }

  start() {
    if (this.timer) {
      this.stop();
    }
    this.video.play();
    this.timer = requestAnimationFrame(this.animate.bind(this));
  }
  
  stop() {
    this.classConfidences = [];
    this.video.pause();
    cancelAnimationFrame(this.timer);
  }
  
  animate() {
    if (this.videoPlaying) {
      // Get image data from video element
      const image = dl.fromPixels(this.video);
      
      // Train class if one of the buttons is held down
      if (this.training != -1) {
        // Add current image to classifier
        this.knn.addImage(image, this.training)
      }
      
      // If any examples have been added, run predict
      const exampleCount = this.knn.getClassExampleCount();
      if (Math.max(...exampleCount) > 0) {
        this.knn.predictClass(image)
        .then((res)=>{
          for (let i = 0; i < NUM_CLASSES; i++) {
            // Make the predicted class bold
            //console.log("PREDICTED CLASS " + res.classIndex);
            if (res.classIndex == i) {
              this.infoTexts[i].style.fontWeight = 'bold';
              if (i > 0 && this.lastClass != i && this.classConfidences[i] >= MIN_CONFIDENCE) {
                //this.iosocket.send(MATERIALS[i]);
                let j = i;
                this.lastClass = j;
                setTimeout(() => {
                  if (this.classConfidences && this.classConfidences[j] >= MIN_CONFIDENCE) {
                    if (this.iosocket == null) return;
                    console.log("Sending material", MATERIAL_MAP[j]);
                    this.iosocket.emit(MATERIAL_MAP[j]);
                  }
                  else if (this.lastClass == j) {
                    this.lastClass = 0;
                  }
                }, 500);
              }
            } else {
              this.infoTexts[i].style.fontWeight = 'normal';
              if (i > 0 && this.lastClass == i) {
                this.lastClass = 0;
              }
            }

            // Update info text
            if (exampleCount[i] > 0) {
              this.infoTexts[i].innerText = ` ${exampleCount[i]} examples - ${res.confidences[i]*100}%`
              this.classConfidences[i] = res.confidences[i];
            }
            else this.classConfidences[i] = 0;
          }
        })
        // Dispose image when done
        .then(()=> image.dispose())
      } else {
        image.dispose()
      }
    }
    this.timer = requestAnimationFrame(this.animate.bind(this));
  }
}


window.addEventListener('load', () => new Main());